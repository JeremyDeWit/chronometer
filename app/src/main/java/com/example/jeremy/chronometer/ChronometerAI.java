package com.example.jeremy.chronometer;

/**
 * Created by Jeremy on 7/10/2016.
 */

public interface ChronometerAI
{
    void onTick();
}
