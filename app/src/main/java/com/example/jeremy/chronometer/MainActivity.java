package com.example.jeremy.chronometer;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements ChronometerAI
{
    private TextView timeView;
    private Button startStopButton;
    private Button resetButton;

    private Chronometer chronometer;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timeView = (TextView) this.findViewById(R.id.timeView);
        startStopButton = (Button) this.findViewById(R.id.startStopButton);
        resetButton = (Button) this.findViewById(R.id.resetButton);
    }

    @Override
    protected  void onPause()
    {
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putLong("accumulatedMilliseconds", chronometer.getAccumulatedMilliseconds());
        editor.putLong("startTimeMillis", chronometer.getStartTimeMillis());
        editor.putLong("startLastPauseMillis", chronometer.getStartLastPauseMillis());
        editor.putLong("totalPausedMillis", chronometer.getTotalPausedMillis());
        editor.putBoolean("isTicking", chronometer.isTicking());
        editor.commit();

        super.onPause();
    }

    @Override
    protected void onResume()
    {
        if (chronometer == null)
            chronometer = new Chronometer(this);

        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);

        chronometer.setAccumulatedMilliseconds(sharedPreference.getLong("accumulatedMilliseconds", 0));
        chronometer.setStartTimeMillis(sharedPreference.getLong("startTimeMillis", 0));
        chronometer.setStartLastPauseMillis(sharedPreference.getLong("startLastPauseMillis", 0));
        chronometer.setTotalPausedMillis(sharedPreference.getLong("totalPausedMillis", 0));
        chronometer.setTicking(sharedPreference.getBoolean("isTicking", false));

        chronometer.resumeChronometerIfNeeded();
        if (chronometer.isTicking())
            startStopButton.setText(R.string.stop);

        updateTimeView();

        super.onResume();
    }

    public void onStartStopClicked(View v)
    {
        chronometer.toggleChronometer();
        if (chronometer.isTicking())
            startStopButton.setText(R.string.stop);
        else
            startStopButton.setText(R.string.start);
    }

    public void onResetClicked(View v)
    {
        chronometer.resetChronometer();
        updateTimeView();
    }

    public TextView getTimeView()
    {
        return timeView;
    }

    public Button getStartStopButton()
    {
        return startStopButton;
    }

    public Button getResetButton()
    {
        return resetButton;
    }

    @Override
    public void onTick()
    {
        updateTimeView();
    }

    private void updateTimeView()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
        calendar.setTimeInMillis(chronometer.getAccumulatedMilliseconds());

        int hour = calendar.get(Calendar.HOUR);
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        int millis = calendar.get(Calendar.MILLISECOND);

        timeView.setText(String.format("%02d:%02d:%02d.%03d", hour, minutes, seconds, millis));
    }
}
