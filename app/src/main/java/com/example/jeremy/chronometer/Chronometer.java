package com.example.jeremy.chronometer;

import android.os.Handler;

/**
 * Created by jeremy on 10/1/2016.
 */

public class Chronometer
{
    private long accumulatedMilliseconds;
    private long startTimeMillis;
    private long totalPausedMillis;
    private long startLastPauseMillis;

    private boolean isTicking;

    private Handler handler;

    private ChronometerAI ai;

    public Chronometer(ChronometerAI chronometerAI)
    {
        accumulatedMilliseconds = 0;
        startTimeMillis = 0;
        totalPausedMillis = 0;
        startLastPauseMillis = 0;
        isTicking = false;

        handler = new Handler();
        ai = chronometerAI;
    }

    public long getAccumulatedMilliseconds()
    {
        return accumulatedMilliseconds;
    }

    public void setAccumulatedMilliseconds(long accumulatedMilliseconds)
    {
        this.accumulatedMilliseconds = accumulatedMilliseconds;
    }

    public long getStartTimeMillis()
    {
        return startTimeMillis;
    }

    public void setStartTimeMillis(long startTimeMillis)
    {
        this.startTimeMillis = startTimeMillis;
    }

    public long getTotalPausedMillis()
    {
        return totalPausedMillis;
    }

    public void setTotalPausedMillis(long totalPausedMillis)
    {
        this.totalPausedMillis = totalPausedMillis;
    }

    public long getStartLastPauseMillis()
    {
        return startLastPauseMillis;
    }

    public void setStartLastPauseMillis(long startLastPauseMillis)
    {
        this.startLastPauseMillis = startLastPauseMillis;
    }

    public boolean isTicking()
    {
        return isTicking;
    }

    public void setTicking(boolean ticking)
    {
        isTicking = ticking;
    }

    public Handler getHandler()
    {
        return handler;
    }

    public void setHandler(Handler handler)
    {
        this.handler = handler;
    }

    public void resetChronometer()
    {
        totalPausedMillis = 0;
        startLastPauseMillis = 0;
        startTimeMillis = isTicking ? System.currentTimeMillis() : 0;
        accumulatedMilliseconds = 0;
    }

    public void toggleChronometer()
    {
        if (isTicking)
        {
            stopChronometer();
        }
        else
        {
            startChronometer();
        }
    }

    public void resumeChronometerIfNeeded()
    {
        if (isTicking)
            startChronometer();
    }

    public void calcAccumulatedMillis()
    {
        setAccumulatedMilliseconds(System.currentTimeMillis() - (startTimeMillis + totalPausedMillis));
    }

    private void scheduleNextTick()
    {
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                tick();
            }
        }, 500);
    }

    private void startChronometer()
    {
        setTicking(true);
        if (startLastPauseMillis != 0)
        {
            totalPausedMillis += System.currentTimeMillis() - startLastPauseMillis;
            startLastPauseMillis = 0;
        }

        if (startTimeMillis == 0)
            startTimeMillis = System.currentTimeMillis();

        scheduleNextTick();
    }

    private void stopChronometer()
    {
        setStartLastPauseMillis(System.currentTimeMillis());
        setTicking(false);

        handler.removeCallbacksAndMessages(null);
    }

    public void tick()
    {
        calcAccumulatedMillis();
        if (ai != null)
            ai.onTick();

        scheduleNextTick();
    }
}
